/**BÀI 1: Sắp xếp 3 số theo thứ tự tăng dần
 *
 * INPUT: n1, n2, n3
 *
 * TODO:
 * Nếu n1>n2 = true thì:
 *  Nếu n2>n3 = true thì:
 *   Xuất ra n3<n2<n1;
 *   else thì:
 *      Nếu n2==n3 = true thì:
 *          Xuất ra n2=n3<n1;
 *          else thì:
 *              n1 > n3 = true thì:
 *                  Xuất ra n2<n3<n1;
 *                  else thì:
 *                      n1 < n3 = true thì:
 *                          Xuất ra n2<n1<n3;
 *                          else Xuất ra n2<n1=n3;
 *  else thì:
 *      n1<n2 = true thì:
 *          Nếu n2<n3 = true thì:
 *              Xuất ra n1<n2<n3;
 *              else thì:
 *                  Nếu n2==n3 = true thì:
 *                      Xuất ra n1<n2=n3;
 *                      else thì:
 *                          nếu n3>n1 = true thì:
 *                              Xuất ra n1<n3<n2;
 *                              else thì:
 *                                  nếu n3<n1 = true thì:
 *                                      Xuất ra n3<n1<n2;
 *                                      else thì n3=n1<n2;
 *      else thì:
 *          nếu n1>n3 = true thì:
 *              Xuất ra n1=n2>n3;
 *              else thì:
 *                  nếu n1<n3 = true thì:
 *                      Xuất ra n1=n2<n3;
 *                      else thì xuất ra n1=n2=n3;
 *
 * OUTPUT:
 * Xuất ra 3 số theo thứ tự tăng dần
 */
function arrangeDigit() {
  //   console.log("Yes");
  var n1 = document.getElementById("txt-n1").value * 1;
  var n2 = document.getElementById("txt-n2").value * 1;
  var n3 = document.getElementById("txt-n3").value * 1;
  var num1st, num2nd, num3rd;
  if (n1 > n2) {
    if (n2 > n3) {
      document.getElementById(
        "ex01-result"
      ).innerHTML = `n3<span class="text-warning fw-semibold">(${n3})</span> < n2<span class="text-warning fw-semibold">(${n2})</span> < n1<span class="text-warning fw-semibold">(${n1})</span>`;
    } else if (n2 == n3) {
      document.getElementById(
        "ex01-result"
      ).innerHTML = `n2<span class="text-warning fw-semibold">(${n2})</span> = n3<span class="text-warning fw-semibold">(${n3})</span> < n1<span class="text-warning fw-semibold">(${n1})</span>`;
    } else if (n1 > n3) {
      document.getElementById(
        "ex01-result"
      ).innerHTML = `n2<span class="text-warning fw-semibold">(${n2})</span> < n3<span class="text-warning fw-semibold">(${n3})</span> < n1<span class="text-warning fw-semibold">(${n1})</span>`;
    } else if (n1 < n3) {
      document.getElementById(
        "ex01-result"
      ).innerHTML = `n2<span class="text-warning fw-semibold">(${n2})</span> < n1<span class="text-warning fw-semibold">(${n1})</span> < n3<span class="text-warning fw-semibold">(${n3})</span>`;
    } else {
      document.getElementById(
        "ex01-result"
      ).innerHTML = `n2<span class="text-warning fw-semibold">(${n2})</span> < n1<span class="text-warning fw-semibold">(${n1})</span> = n3<span class="text-warning fw-semibold">(${n3})</span>`;
    }
  } else if (n1 < n2) {
    if (n2 < n3) {
      document.getElementById(
        "ex01-result"
      ).innerHTML = `n1<span class="text-warning fw-semibold">(${n1})</span> < n2<span class="text-warning fw-semibold">(${n2})</span> < n3<span class="text-warning fw-semibold">(${n3})</span>`;
    } else if (n2 == n3) {
      document.getElementById(
        "ex01-result"
      ).innerHTML = `n1<span class="text-warning fw-semibold">(${n1})</span> < n2<span class="text-warning fw-semibold">(${n2})</span> = n3<span class="text-warning fw-semibold">(${n3})</span>`;
    } else if (n3 > n1) {
      document.getElementById(
        "ex01-result"
      ).innerHTML = `n1<span class="text-warning fw-semibold">(${n1})</span> < n3<span class="text-warning fw-semibold">(${n3})</span> < n2<span class="text-warning fw-semibold">(${n2})</span>`;
    } else if (n3 < n1) {
      document.getElementById(
        "ex01-result"
      ).innerHTML = `n3<span class="text-warning fw-semibold">(${n3})</span> < n1<span class="text-warning fw-semibold">(${n1})</span> < n2<span class="text-warning fw-semibold">(${n2})</span>`;
    } else {
      document.getElementById(
        "ex01-result"
      ).innerHTML = `n3<span class="text-warning fw-semibold">(${n3})</span> = n1<span class="text-warning fw-semibold">(${n1})</span> < n2<span class="text-warning fw-semibold">(${n2})</span>`;
    }
  } else if (n1 > n3) {
    document.getElementById(
      "ex01-result"
    ).innerHTML = `n1<span class="text-warning fw-semibold">(${n1})</span> = n2<span class="text-warning fw-semibold">(${n2})</span> > n3<span class="text-warning fw-semibold">(${n3})</span>`;
  } else if (n1 < n3) {
    document.getElementById(
      "ex01-result"
    ).innerHTML = `n1<span class="text-warning fw-semibold">(${n1})</span> - n2<span class="text-warning fw-semibold">(${n2})</span> < n3<span class="text-warning fw-semibold">(${n3})</span>`;
  } else {
    document.getElementById(
      "ex01-result"
    ).innerHTML = `n1<span class="text-warning fw-semibold">(${n1})</span> = n2<span class="text-warning fw-semibold">(${n2})</span> = n3<span class="text-warning fw-semibold">(${n3})</span>`;
  }
}
// END BÀI 1

/** BÀI 2: Viết chương tình chào hỏi thành viên trong gia đình
 *
 * INPUT:
 * Select user: Bố, Mẹ, Anh Trai, Em Gái
 *
 * TODO:
 * Nhấn Button (Đăng nhập) ra lời chào theo Select User bên trên.
 *
 * OUTPUT:
 * Chào Bố, Chào Mẹ, Chào Anh Trai, Chào Em Gái.
 */

var sayHelloUser = function () {
  var username = document.getElementById("ten-dang-nhap").value;
  switch (username) {
    case "B":
      username = "Bố";
      break;
    case "M":
      username = "Mẹ";
      break;
    case "A":
      username = "Anh Trai";
      break;
    case "E":
      username = "Em Gái";
      break;
    default:
      username = "Unknown";
      break;
  }
  switch (username) {
    case "Unknown":
      document.getElementById(
        "welcome-message"
      ).innerHTML = `Welcome Back <h3 class="d-inline text-warning">Sir. Anonymous!!</h3>`;
      break;
    default:
      document.getElementById(
        "welcome-message"
      ).innerHTML = `Chào <h3 class="d-inline text-warning">${username}</h3> đã về!!!`;
      break;
  }
};
// END BÀI 2

/** BÀI 3: Cho 3 số nguyên. Viết chương trình xuất ra có bao nhiêu số lẻ, bao nhiêu số chẵn
 * INPUT:
 * n1, n2, n3 (số nguyên)
 *
 * TODO:
 * var oddNum = 0;
 * var evenNum = 0;
 *
 * function (n)
 * nếu n % 2 = 1 thì oddNum = oddNum + 1;
 * ngược lại evenNum = evenNum + 1;
 *
 * Chạy function với n1,n2,n3;
 *
 * OUTPUT:
 * oddNum, evenNum
 */
function countEvenOdd() {
  // console.log("Yes");
  var n1 = document.getElementById("txt-ex03-n1").value * 1;
  var n2 = document.getElementById("txt-ex03-n2").value * 1;
  var n3 = document.getElementById("txt-ex03-n3").value * 1;

  var oddNum = 0;
  var evenNum = 0;
  function checkOddEven(n) {
    n % 2 == 1 && oddNum++;
    n % 2 == 0 && evenNum++;
  }
  checkOddEven(n1);
  checkOddEven(n2);
  checkOddEven(n3);

  document.getElementById(
    "ex03-result"
  ).innerHTML = `Số lẻ: <h3 class="d-inline text-warning">${oddNum}</h3> số <span class="text-danger fs-2">|</span> Số chẵn: <h3 class="d-inline text-warning">${evenNum}</h3> số`;
}
// END BÀI 3

/** BÀI 4: Cho nhập 3 cạnh của tam giác. Đó là tam giác gì?
 * INPUT:
 * 3 canh tam giác: a,b,c
 *
 * TODO:
 * var triangleType
 *
 * nếu a=b=c thì là TAM GIÁC ĐỀU
 * nếu a=b hoặc b=c hoặc a=c thì
 *  nếu a^2 + b^2 = c^2 hoặc a^2 + c^2 = b^2 hoặc b^2 + c^2 = a^2 thì TAM GIÁC VUÔNG CÂN
 *  ngược lại thì TAM GIÁC CÂN
 * nếu a^2 + b^2 = c^2 hoặc a^2 + c^2 = b^2 hoặc b^2 + c^2 = a^2 thì TAM GIÁC VUÔNG
 * ngược lại thì TAM GIÁC THƯỜNG
 *
 * OUTPUT:
 * triangleType = loại tam giác.
 */

function checkTriangleType() {
  // console.log("yes");
  var sideA = document.getElementById("txt-ex04-a").value * 1;
  var sideB = document.getElementById("txt-ex04-b").value * 1;
  var sideC = document.getElementById("txt-ex04-c").value * 1;
  var triangleType = null;

  function checkRightTriangle(a, b, c) {
    var isRightTriangle =
      a ** 2 + b ** 2 == c ** 2 ||
      a ** 2 + c ** 2 == b ** 2 ||
      b ** 2 + c ** 2 == a ** 2;
    return isRightTriangle;
  }

  if (sideA == sideB && sideB == sideC) {
    triangleType = "TAM GIÁC ĐỀU";
  } else if (sideA == sideB || sideA == sideC || sideB == sideC) {
    if (checkRightTriangle(sideA, sideB, sideC)) {
      triangleType = "TAM GIÁC VUÔNG CÂN";
    } else {
      triangleType = "TAM GIÁC CÂN";
    }
  } else if (checkRightTriangle(sideA, sideB, sideC)) {
    triangleType = "TAM GIÁC VUÔNG";
  } else {
    triangleType = "TAM GIÁC THƯỜNG";
  }

  document.getElementById(
    "ex04-result"
  ).innerHTML = `Là <span class="text-warning fw-semibold">${triangleType}</span> nha!!`;
}
